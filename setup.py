#!/usr/bin/env python
# Copyright (C) 2010 Linaro Limited
#
# Author: Zygmunt Krynicki <zygmunt.krynicki@linaro.org>
#
# This file is part of django-restricted-resource.
#
# django-restricted-resource is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# django-restricted-resource is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with django-restricted-resource.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup


setup(
    name="django-restricted-resource",
    version="2016.8",
    author="Linaro Limited",
    author_email="lava-team@linaro.org",
    description="Base model for Django that adds simple and efficient ownership and access control.",
    url="http://git.linaro.org/git/lava/django-restricted-resource.git",
    test_suite="django_restricted_resource.test_project.tests.run_tests",
    license="LGPLv3",
    keywords=["django", "ownership", "models"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.5",
    ],
    zip_safe=True,
    packages=["django_restricted_resource"],
    # dependencies
    install_requires=["django >= 1.7"],
    tests_require=["django-testscenarios >= 0.9"],
)
